import string, random, re

ALPHABETNUMBER = 26
MAXITERATION = 1000

def cleansing(text):
    text = text.lower()
    words = re.sub(r"[^a-z ]+", ' ', text).split(" ")
    words = set(words)
    return list(words)

def preProcess(globalFileAddress, encodedText):
    with open(globalFileAddress) as file:
        text = file.read()
    dictionary = cleansing(text)
    encodedDictionary = cleansing(encodedText)

    return dictionary, encodedDictionary

def intiatePopulation(populationSize):
    population = list()
    for _ in range(populationSize):
        randomSequence = random.sample(string.ascii_lowercase, ALPHABETNUMBER)
        keyMapping = dict(zip(string.ascii_lowercase, randomSequence))
        population.append((keyMapping, 0))
    
    return population

def decodeWord(key, word):
    decoded = []
    for char in word:
        decoded.append(key[char])
    
    return "".join(decoded)

def decode(key, encodedDictionary):
    decodedDictionary = list()
    for word in encodedDictionary:
        decodedDictionary.append(decodeWord(key, word))
    return decodedDictionary

def fittingFunction(decoded, dictionary):
    score = 0
    hasMissed = False
    for word in decoded:
        if word in dictionary:
            score += len(word)
        else:
            hasMissed = True
    return score, hasMissed

def fitness(key, encodedDictionary, dictionary):
    decoded = decode(key, encodedDictionary)
    return fittingFunction(decoded, dictionary)

def calculateFitness(population, encodedDictionary, dictionary):
    newPopulation = []
    bestFit = None
    for key in population:
        score, hasMissed = fitness(key[0], encodedDictionary, dictionary)
        if not hasMissed:
            bestFit = key
        newPopulation.append((key[0], score))

    if bestFit is None:
        bestFit = max(newPopulation, key=lambda element: element[1])
    return newPopulation, bestFit, hasMissed

def makeMatingPool(population):
    population.sort(key=lambda element: element[1])
    rankProbability = []
    sumRanks = 1
    for i in range(len(population)):
        rankProbability.append(sumRanks)
        sumRanks += (i+1)
    matingPool = random.choices(population, cum_weights=rankProbability, k=len(population))
    return matingPool

def refillDuplicate(key):
    unique = set(key)
    alphaSet = set(string.ascii_lowercase)
    notUsed = list(alphaSet - unique)
    random.shuffle(notUsed)
    ite = 0
    n = len(key)
    for i in range(n):
        for j in range(i+1, n):
            if key[i] == key[j]:
                key[j] = notUsed[ite]
                ite += 1

def crossOver(parent1, parent2, numCrossOver):
    n = len(parent1[0])
    crossPoints = random.sample(list(range(n)), k=numCrossOver)
    crossPoints.sort()
    key1 = list(parent1[0].values())
    key2 = list(parent2[0].values())
    ite = 0
    childFlag = True
    childKey1 = []
    childKey2 = []
    for index in range(n):
        if ite < len(crossPoints) and index == crossPoints[ite]:
            ite += 1
            childFlag = not childFlag
        if childFlag:
            
            childKey1.append(key1[index])
            childKey2.append(key2[index])
        else:
            childKey1.append(key2[index])
            childKey2.append(key1[index])
    refillDuplicate(childKey1)
    refillDuplicate(childKey2)
    child1 = dict(zip(string.ascii_lowercase, childKey1))
    child2 = dict(zip(string.ascii_lowercase, childKey2))
    return child1, child2

def makeNextGeneration(matingPool, probCrossOver, numCrossOver, probMutation):
    random.shuffle(matingPool)
    population = list()
    for i in range(0, len(matingPool), 2):
        if random.uniform(0, 1) < probCrossOver:
            child1, child2 = crossOver(matingPool[i], matingPool[i+1], numCrossOver)
            if random.uniform(0, 1) < probMutation:
                child1 = mutate(child1)
            if random.uniform(0, 1) < probMutation:
                child2 = mutate(child2)
            population.append((child1, 0))
            population.append((child2, 0))
        else:
            population.append(matingPool[i])
            population.append(matingPool[i+1])
    
    return population

def mutate(key):
    key1 = random.choice(list(key.keys()))
    key2 = random.choice(list(key.keys()))
    val1 = key[key1]
    val2 = key[key2]
    key[key1] = val2
    key[key2] = val1
    return key

def geneticAlgorithm(dictionary, encodedDictionary, populationSize, probCrossOver, numCrossOver, probMutation):
    population = intiatePopulation(populationSize)
    population, bestFit, hasMissed = calculateFitness(population, encodedDictionary, dictionary)
    numberOfIteration = 0
    while hasMissed and numberOfIteration < MAXITERATION:
        matingPool = makeMatingPool(population)
        population = makeNextGeneration(matingPool, probCrossOver, numCrossOver, probMutation)
        population, bestFit, hasMissed = calculateFitness(population, encodedDictionary, dictionary)
        numberOfIteration += 1

    return bestFit

def decodeText(text, key):
    decoded = ""
    for char in text:
        if char.isupper():
            decoded += key[char.lower()].upper()
        elif char.islower():
            decoded += key[char]
        else:
            decoded += char
    return decoded

def runDecodeProcess(globalFileAddress, encodedText, populationSize, probCrossOver, numCrossOver, probMutation):
    dictionary, encodedDictionary = preProcess(globalFileAddress, encodedText)
    bestFit = geneticAlgorithm(dictionary, encodedDictionary, populationSize, probCrossOver, numCrossOver, probMutation)
    return decodeText(encodedText, bestFit[0])
    

class Decoder():
    def __init__(self, encodedText, globalFileAddress="global_text.txt", populationSize=200, probCrossOver=0.7, numCrossOver=6, probMutation=0.1):
        self.encodedText = encodedText
        self.globalFileAddress = globalFileAddress
        self.populationSize = populationSize
        self.probCrossOver = probCrossOver
        self.numCrossOver = numCrossOver
        self.probMutation = probMutation

    def decode(self):
        return runDecodeProcess(self.globalFileAddress, self.encodedText, self.populationSize, self.probCrossOver, self.numCrossOver, self.probMutation)
    
