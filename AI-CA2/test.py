from time import time
from code import Decoder

encoded_text = open("encoded_text.txt").read()
d = Decoder(encoded_text)
start = time()
decoded_text = d.decode()
print(decoded_text)
end = time()

print("Time: " + str(end - start) + "s")